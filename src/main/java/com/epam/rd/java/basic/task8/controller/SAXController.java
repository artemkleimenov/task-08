package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constants.Constants;
import com.epam.rd.java.basic.task8.constants.XML;
import com.epam.rd.java.basic.task8.entity.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Objects;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private String xmlFileName;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // PLACE YOUR CODE HERE
    /**
     * Current element name holder
     */
    private String currentElement;
    /**
     * Container
     */
    private Flowers flowers;
    private Flower flower;
    private GrowingTips growingTips;
    private VisualParameters visualParameters;

    public Flowers getFlowers() {
        return flowers;
    }

    public void parse(boolean validate) throws SAXException, ParserConfigurationException, IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance();

        factory.setFeature(Constants.FEATURE_DISALLOW_DOCTYPE_DECL, true);
        factory.setNamespaceAware(true);

        if (validate) {
            factory.setFeature(Constants.FEATURE_TURN_VALIDATION_ON, true);
            factory.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
        }

        javax.xml.parsers.SAXParser parser = factory.newSAXParser();
        parser.parse(xmlFileName, this);
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        currentElement = localName;

        if (Objects.equals(currentElement, XML.FLOWERS.value())) {
            flowers = new Flowers();
        } else if (Objects.equals(currentElement, XML.FLOWER.value())) {
            flower = new Flower();
        } else if (Objects.equals(currentElement, XML.VISUALPARAMETERS.value())) {
            visualParameters = new VisualParameters();
        } else if (Objects.equals(currentElement, XML.GROWINGTIPS.value())) {
            growingTips = new GrowingTips();
        } else if (Objects.equals(currentElement, XML.AVELENFLOWER.value())) {
            visualParameters.setAveLenFlower(new AveLenFlower());

            if (attributes.getLength() > 0) {
                visualParameters.getAveLenFlower().setMeasure(attributes.getValue(uri, XML.MEASURE.value()));
            }
        } else
            setGrowingTips(uri, attributes);
    }

    /**
     * Receive notification of the end of an element.
     *
     * <p>By default, do nothing.  Application writers may override this
     * method in a subclass to take specific actions at the end of
     * each element (such as finalising a tree node or writing
     * output to a file).</p>
     *
     * @param uri       The Namespace URI, or the empty string if the
     *                  element has no Namespace URI or if Namespace
     *                  processing is not being performed.
     * @param localName The local name (without prefix), or the
     *                  empty string if Namespace processing is not being
     *                  performed.
     * @param qName     The qualified name (with prefix), or the
     *                  empty string if qualified names are not available.
     * @throws SAXException Any SAX exception, possibly
     *                      wrapping another exception.
     * @see ContentHandler#endElement
     */
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (Objects.equals(localName, XML.FLOWER.value())) {
            flowers.getFlowers().add(flower);
        } else if (Objects.equals(localName, XML.VISUALPARAMETERS.value())) {
            flower.setVisualParameters(visualParameters);
        } else if (Objects.equals(localName, XML.GROWINGTIPS.value())) {
            flower.setGrowingTips(growingTips);
        }
    }

    /**
     * Receive notification of character data inside an element.
     *
     * <p>By default, do nothing.  Application writers may override this
     * method to take specific actions for each chunk of character data
     * (such as adding the data to a node or buffer, or printing it to
     * a file).</p>
     *
     * @param ch     The characters.
     * @param start  The start position in the character array.
     * @param length The number of characters to use from the
     *               character array.
     * @throws SAXException Any SAX exception, possibly
     *                      wrapping another exception.
     * @see ContentHandler#characters
     */

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String elem = new String(ch, start, length).trim();

        if (elem.isEmpty()) {
            return;
        }

        if (Objects.equals(currentElement, XML.NAME.value())) {
            flower.setName(elem);
        } else if (Objects.equals(currentElement, XML.SOIL.value())) {
            flower.setSoil(elem);
        } else if (Objects.equals(currentElement, XML.ORIGIN.value())) {
            flower.setOrigin(elem);
        } else if (Objects.equals(currentElement, XML.STEAMCOLOUR.value())) {
            visualParameters.setStemColour(elem);
        } else if (Objects.equals(currentElement, XML.LEAFCOLOUR.value())) {
            visualParameters.setLeafColour(elem);
        } else if (Objects.equals(currentElement, XML.AVELENFLOWER.value())) {
            visualParameters.getAveLenFlower().setValue(BigInteger.valueOf(Integer.parseInt(elem)));
        } else if (Objects.equals(currentElement, XML.TEMPERATURE.value())) {
            growingTips.getTemperature().setValue(BigInteger.valueOf(Integer.parseInt(elem)));
        } else if (Objects.equals(currentElement, XML.WATERING.value())) {
            growingTips.getWatering().setValue(BigInteger.valueOf(Integer.parseInt(elem)));
        } else if (Objects.equals(currentElement, XML.MULTIPLYING.value())) {
            flower.setMultiplying(elem);
        }
    }

    private void setGrowingTips(String uri, Attributes attributes) {
        if (Objects.equals(currentElement, XML.TEMPERATURE.value())) {
            growingTips.setTemperature(new Temperature());

            if (attributes.getLength() > 0) {
                growingTips.getTemperature().setMeasure(attributes.getValue(uri, XML.MEASURE.value()));
            }
        } else if (Objects.equals(currentElement, XML.LIGHTNING.value())) {
            growingTips.setLighting(new Lighting());

            if (attributes.getLength() > 0) {
                growingTips.getLighting().setLightRequiring(attributes.getValue(0));
            }
        } else if (Objects.equals(currentElement, XML.WATERING.value())) {
            growingTips.setWatering(new Watering());

            if (attributes.getLength() > 0) {
                growingTips.getWatering().setMeasure(attributes.getValue(uri, XML.WATERING.value()));
            }
        }
    }
}