package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constants.XML;
import com.epam.rd.java.basic.task8.entity.*;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import javax.xml.transform.stream.StreamSource;
import java.math.BigInteger;
import java.util.Objects;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private String xmlFileName;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // PLACE YOUR CODE HERE
    /**
     * Current element name holder
     */
    private String currentElement;
    /**
     * Container
     */
    private Flowers flowers;
    private Flower flower;
    private GrowingTips growingTips;
    private VisualParameters visualParameters;

    public Flowers getFlowers() {
        return flowers;
    }

    public void parse() throws XMLStreamException {
        XMLInputFactory factory = XMLInputFactory.newInstance();

        factory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
        factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);

        XMLEventReader reader = factory.createXMLEventReader(new StreamSource(xmlFileName));

        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();

            if (event.isCharacters() && event.asCharacters().isWhiteSpace()) {
                continue;
			}

            if (event.isStartElement()) {
                StartElement startElement = event.asStartElement();
                currentElement = startElement.getName().getLocalPart();
                startTags(startElement);
            }

            if (event.isCharacters()) {
                Characters characters = event.asCharacters();
                contentHandler(characters);
            }

            if (event.isEndElement()) {
                EndElement endElement = event.asEndElement();
                String localName = endElement.getName().getLocalPart();
                endTags(localName);
            }
        }

        reader.close();
    }

    /**
     * Handler for start tags.
     * @param startElement
     */
    public void startTags(StartElement startElement) {
        if (Objects.equals(currentElement, XML.FLOWERS.value())) {
            flowers = new Flowers();
        } else if (Objects.equals(currentElement, XML.FLOWER.value())) {
            flower = new Flower();
        } else if (Objects.equals(currentElement, XML.VISUALPARAMETERS.value())) {
            visualParameters = new VisualParameters();
        } else if (Objects.equals(currentElement, XML.GROWINGTIPS.value())) {
            growingTips = new GrowingTips();
        } else if (Objects.equals(currentElement, XML.AVELENFLOWER.value())) {
            visualParameters.setAveLenFlower(new AveLenFlower());
            Attribute attribute = startElement.getAttributeByName(new QName(XML.MEASURE.value()));

            if (attribute != null) {
                visualParameters.getAveLenFlower().setMeasure(attribute.getValue());
            }
        } else {
            startTagsGrowingTips(startElement);
        }
    }

    public void startTagsGrowingTips(StartElement startElement) {
        if (Objects.equals(currentElement, XML.TEMPERATURE.value())) {
            growingTips.setTemperature(new Temperature());
            Attribute attribute = startElement.getAttributeByName(new QName(XML.MEASURE.value()));

			if (attribute != null) {
                growingTips.getTemperature().setMeasure(attribute.getValue());
			}
        } else if (Objects.equals(currentElement, XML.LIGHTNING.value())) {
            growingTips.setLighting(new Lighting());

			Attribute attribute = startElement.getAttributeByName(new QName(XML.LIGHTREQUIRING.value()));

			if (attribute != null) {
                growingTips.getLighting().setLightRequiring(attribute.getValue());
			}
        } else if (Objects.equals(currentElement, XML.WATERING.value())) {
            growingTips.setWatering(new Watering());

            Attribute attribute = startElement.getAttributeByName(new QName(XML.MEASURE.value()));

			if (attribute != null) {
                growingTips.getWatering().setMeasure(attribute.getValue());
			}
        }
    }

    /**
     * Handler for end tags.
     * @param localName
     */
    public void endTags(String localName) {
        if (Objects.equals(localName, XML.FLOWER.value())) {
            flowers.getFlowers().add(flower);
        } else if (Objects.equals(localName, XML.VISUALPARAMETERS.value())) {
            flower.setVisualParameters(visualParameters);
        } else if (Objects.equals(localName, XML.GROWINGTIPS.value())) {
            flower.setGrowingTips(growingTips);
        }
    }

    /**
     * Handler for contents.
     * @param characters
     */
    public void contentHandler(Characters characters) {
        if (Objects.equals(currentElement, XML.NAME.value())) {
            flower.setName(characters.getData());
        } else if (Objects.equals(currentElement, XML.SOIL.value())) {
            flower.setSoil(characters.getData());
        } else if (Objects.equals(currentElement, XML.ORIGIN.value())) {
            flower.setOrigin(characters.getData());
        } else if (Objects.equals(currentElement, XML.STEAMCOLOUR.value())) {
            visualParameters.setStemColour(characters.getData());
        } else if (Objects.equals(currentElement, XML.LEAFCOLOUR.value())) {
            visualParameters.setLeafColour(characters.getData());
        } else if (Objects.equals(currentElement, XML.AVELENFLOWER.value())) {
            visualParameters.getAveLenFlower().setValue(BigInteger.valueOf(Integer.parseInt(characters.getData())));
        } else if (Objects.equals(currentElement, XML.TEMPERATURE.value())) {
            growingTips.getTemperature().setValue(BigInteger.valueOf(Integer.parseInt(characters.getData())));
        } else if (Objects.equals(currentElement, XML.WATERING.value())) {
            growingTips.getWatering().setValue(BigInteger.valueOf(Integer.parseInt(characters.getData())));
        } else if (Objects.equals(currentElement, XML.MULTIPLYING.value())) {
            flower.setMultiplying(characters.getData());
        }
    }
}