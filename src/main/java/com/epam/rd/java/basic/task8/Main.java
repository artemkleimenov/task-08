package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.DOMController;
import com.epam.rd.java.basic.task8.controller.SAXController;
import com.epam.rd.java.basic.task8.controller.STAXController;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.util.Sorting;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		domController.parse(true);
		// sort (case 1)
		// PLACE YOUR CODE HERE
		Flowers flowers = domController.getFlowers();
		Sorting.sortFlowersByAveLen(flowers);
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		DOMController.saveXML(flowers, outputXmlFile);
		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		saxController.parse(true);
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		flowers = saxController.getFlowers();
		Sorting.sortFlowersByFlowersName(flowers);
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		DOMController.saveXML(flowers, outputXmlFile);
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////

		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		staxController.parse();
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		flowers = staxController.getFlowers();
		Sorting.sortFlowersByAveLen(flowers);
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		DOMController.saveXML(flowers, outputXmlFile);
	}

}
