package com.epam.rd.java.basic.task8.constants;

public enum XML {
    FLOWERS("flowers"),
    FLOWER("flower"),
    NAME("name"),
    SOIL("soil"),
    ORIGIN("origin"),
    VISUALPARAMETERS("visualParameters"),
    STEAMCOLOUR("stemColour"),
    LEAFCOLOUR("leafColour"),
    AVELENFLOWER("aveLenFlower"),
    GROWINGTIPS("growingTips"),
    TEMPERATURE("tempreture"),
    LIGHTNING("lighting"),
    WATERING("watering"),
    MULTIPLYING("multiplying"),
    MEASURE("measure"),
    LIGHTREQUIRING("lightRequiring");

    private String value;

    XML(String value) {
        this.value = value.intern();
    }

    public String value() {
        return value;
    }
}
