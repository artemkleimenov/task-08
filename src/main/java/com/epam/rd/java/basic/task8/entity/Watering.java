package com.epam.rd.java.basic.task8.entity;

import java.math.BigInteger;

public class Watering {
    private BigInteger value;
    private String measure;

    public BigInteger getValue() {
        return value;
    }

    public void setValue(BigInteger value) {
        this.value = value;
    }

    public String getMeasure() {
        if (this.measure == null) {
            return "mlPerWeek";
        } else {
            return measure;
        }
    }

    public void setMeasure(String value) {
        this.measure = value;
    }
}
