package com.epam.rd.java.basic.task8.util;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flowers;

import java.util.Collections;
import java.util.Comparator;

public class Sorting {
    private Sorting() {
    }

    public static void sortFlowersByFlowersName(Flowers flowers){
        Collections.sort(flowers.getFlowers(), Comparator.comparing(Flower::getName));
    }

    public static void sortFlowersByAveLen(Flowers flowers){
        Collections.sort(flowers.getFlowers(), Comparator.comparing(o -> o.getVisualParameters().getAveLenFlower().getValue()));
    }

    public static void sortFlowersByWateringMeasure(Flowers flowers){
        Collections.sort(flowers.getFlowers(), Comparator.comparing(o -> o.getGrowingTips().getWatering().getValue()));
    }
}
