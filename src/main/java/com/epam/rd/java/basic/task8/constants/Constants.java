package com.epam.rd.java.basic.task8.constants;

public class Constants {
    private Constants() {
    }

    /**
     * True: Validate the document and report validity errors.
     * False: Do not report validity errors.
     */
    public static final String FEATURE_TURN_VALIDATION_ON = "http://xml.org/sax/features/validation";
    /**
     * True: A fatal error is thrown if the incoming document contains a DOCTYPE declaration.
     * False: DOCTYPE declaration is allowed.
     */
    public static final String FEATURE_DISALLOW_DOCTYPE_DECL = "http://apache.org/xml/features/disallow-doctype-decl";
    /**
     * True: Turn on XML Schema support.
     * False: Turn off XML Schema support.
     */
    public static final String FEATURE_TURN_SCHEMA_VALIDATION_ON = "http://apache.org/xml/features/validation/schema";
}
