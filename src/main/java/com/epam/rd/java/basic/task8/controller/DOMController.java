package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constants.Constants;
import com.epam.rd.java.basic.task8.constants.XML;
import com.epam.rd.java.basic.task8.entity.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	/**
	 * Container
	 */
	private Flowers flowers;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	public Flowers getFlowers() {
		return flowers;
	}

	public void parse(boolean validate) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		dbf.setFeature(Constants.FEATURE_DISALLOW_DOCTYPE_DECL, true);
		dbf.setNamespaceAware(true);

		if (validate) {
			dbf.setFeature(Constants.FEATURE_TURN_VALIDATION_ON, true);
			dbf.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
		}

		DocumentBuilder db = dbf.newDocumentBuilder();

		db.setErrorHandler(new DefaultHandler() {
			@Override
			public void error(SAXParseException e) throws SAXException {
				throw e;
			}
		});

		Document document = db.parse(xmlFileName);
		Element root = document.getDocumentElement();

		flowers = new Flowers();

		NodeList flowerNodes = root.getElementsByTagName(XML.FLOWER.value());

		for (int j = 0; j < flowerNodes.getLength(); j++) {
			Flower flower = getFlower(flowerNodes.item(j));
			flowers.getFlowers().add(flower);
		}
	}

	public static void saveXML(Flowers flowers, String xmlFileName) throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		dbf.setFeature(Constants.FEATURE_DISALLOW_DOCTYPE_DECL, true);
		dbf.setNamespaceAware(true);

		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.newDocument();

		Element flowersElement = document.createElement(XML.FLOWERS.value());
		document.appendChild(flowersElement);

		flowersElement.setAttribute("xmlns", "http://www.nure.ua");
		flowersElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		flowersElement.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");

		for (Flower flower : flowers.getFlowers()){
			//flower
			Element flowerElem = document.createElement(XML.FLOWER.value());
			flowersElement.appendChild(flowerElem);

			//name
			appendChild(document, flowerElem, XML.NAME.value(), flower.getName());

			//soil
			appendChild(document, flowerElem, XML.SOIL.value(), flower.getSoil());

			//origin
			appendChild(document, flowerElem, XML.ORIGIN.value(), flower.getOrigin());

			//visual parameters
			Element visualParamElem = document.createElement(XML.VISUALPARAMETERS.value());
			flowerElem.appendChild(visualParamElem);

			//stemColour
			appendChild(document, visualParamElem, XML.STEAMCOLOUR.value(), flower.getVisualParameters().getStemColour());

			//leafColour
			appendChild(document, visualParamElem, XML.LEAFCOLOUR.value(), flower.getVisualParameters().getLeafColour());

			//aveLenFlower
			appendChild(document,
					visualParamElem,
					XML.AVELENFLOWER.value(),
					flower.getVisualParameters().getAveLenFlower().getValue().toString(),
					XML.MEASURE.value(),
					flower.getVisualParameters().getAveLenFlower().getMeasure());

			//growingTips
			Element growingTipsElem = document.createElement(XML.GROWINGTIPS.value());
			flowerElem.appendChild(growingTipsElem);

			//temperature
			appendChild(document,
					growingTipsElem,
					XML.TEMPERATURE.value(),
					flower.getGrowingTips().getTemperature().getValue().toString(),
					XML.MEASURE.value(),
					flower.getGrowingTips().getTemperature().getMeasure());

			//lighting
			appendChild(document,
					growingTipsElem,
					XML.LIGHTNING.value(),
					XML.LIGHTREQUIRING.value(),
					flower.getGrowingTips().getLighting().getLightRequiring());

			//watering
			appendChild(document,
					growingTipsElem,
					XML.WATERING.value(),
					flower.getGrowingTips().getWatering().getValue().toString(),
					XML.MEASURE.value(),
					flower.getGrowingTips().getWatering().getMeasure());

			//multiplying
			appendChild(document, flowerElem, XML.MULTIPLYING.value(), flower.getMultiplying());
		}

		DOMController.saveToXML(document, xmlFileName);
	}

	private static void appendChild(Document d, Element parent, String childName, String childText) {
		Element e = d.createElement(childName);
		e.setTextContent(childText);
		parent.appendChild(e);
	}

	private static void appendChild(Document d, Element parent, String childName, String attrName, String attrText) {
		Element e = d.createElement(childName);
		e.setAttribute(attrName, attrText);
		parent.appendChild(e);
	}

	private static void appendChild(Document d, Element parent, String childName, String childText, String attrName, String attrText) {
		Element e = d.createElement(childName);
		e.setTextContent(childText);
		e.setAttribute(attrName, attrText);
		parent.appendChild(e);
	}

	private static Flower getFlower(Node node) {
		Flower flower = new Flower();
		Element flowerElem = (Element) node;

		Node name = flowerElem.getElementsByTagName(XML.NAME.value()).item(0);
		flower.setName(name.getTextContent());

		Node soil = flowerElem.getElementsByTagName(XML.SOIL.value()).item(0);
		flower.setSoil(soil.getTextContent());

		Node origin = flowerElem.getElementsByTagName(XML.ORIGIN.value()).item(0);
		flower.setOrigin(origin.getTextContent());

		flower.setVisualParameters(getVisualParameters(node));
		flower.setGrowingTips(getGrowingTips(node));
		flower.setMultiplying(flowerElem.getElementsByTagName(XML.MULTIPLYING.value()).item(0).getTextContent());

		return flower;
	}

	private static VisualParameters getVisualParameters(Node node){
		VisualParameters visualParameters = new VisualParameters();
		Element vpElement = (Element) node;
		Node steamColour = vpElement.getElementsByTagName(XML.STEAMCOLOUR.value()).item(0);

		visualParameters.setStemColour(steamColour.getTextContent());

		Node leafColour = vpElement.getElementsByTagName(XML.LEAFCOLOUR.value()).item(0);
		visualParameters.setLeafColour(leafColour.getTextContent());
		visualParameters.setAveLenFlower(getAveLenFlower(node));

		return visualParameters;
	}

	private static AveLenFlower getAveLenFlower(Node node){
		AveLenFlower aveLenFlower = new AveLenFlower();
		Element alfElement = (Element) node;
		Node alfNode = alfElement.getElementsByTagName(XML.AVELENFLOWER.value()).item(0);

		aveLenFlower.setValue(BigInteger.valueOf(Integer.parseInt(alfNode.getTextContent())));
		aveLenFlower.setMeasure(alfNode.getAttributes().item(0).getTextContent());

		return  aveLenFlower;
	}

	private static GrowingTips getGrowingTips(Node node){
		GrowingTips growingTips = new GrowingTips();
		Element gtElement = (Element) node;
		Node gtNode = gtElement.getElementsByTagName(XML.TEMPERATURE.value()).item(0);

		growingTips.setTemperature(new Temperature());
		growingTips.getTemperature().setValue(BigInteger.valueOf(Integer.parseInt(gtNode.getTextContent())));
		growingTips.getTemperature().setMeasure(gtNode.getAttributes().item(0).getTextContent());

		gtNode = gtElement.getElementsByTagName(XML.LIGHTNING.value()).item(0);
		growingTips.setLighting(new Lighting());
		growingTips.getLighting().setLightRequiring(gtNode.getAttributes().item(0).getTextContent());

		gtNode = gtElement.getElementsByTagName(XML.WATERING.value()).item(0);
		growingTips.setWatering(new Watering());
		growingTips.getWatering().setValue(BigInteger.valueOf(Integer.parseInt(gtNode.getTextContent())));
		growingTips.getWatering().setMeasure(gtNode.getAttributes().item(0).getTextContent());

		return growingTips;
	}

	/**
	 * Save xml file on disk.
	 * @param document
	 * @param xmlFileName
	 * @throws TransformerException
	 */
	private static void saveToXML(Document document, String xmlFileName) throws TransformerException {
		StreamResult result = new StreamResult(new File(xmlFileName));
		TransformerFactory tf = TransformerFactory.newInstance();

		tf.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
		tf.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");

		javax.xml.transform.Transformer t = tf.newTransformer();
		t.setOutputProperty(OutputKeys.INDENT, "yes");

		t.transform(new DOMSource(document), result);
	}
}
