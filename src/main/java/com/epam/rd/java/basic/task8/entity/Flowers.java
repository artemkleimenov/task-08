package com.epam.rd.java.basic.task8.entity;

import java.util.ArrayList;
import java.util.List;

public class Flowers {
    private List<Flower> flowers;

    public List<Flower> getFlowers() {
        if (this.flowers == null) {
            flowers = new ArrayList<>();
        }

        return flowers;
    }
}
