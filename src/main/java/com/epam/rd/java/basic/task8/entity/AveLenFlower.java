package com.epam.rd.java.basic.task8.entity;

import java.math.BigInteger;

public class AveLenFlower {
    private BigInteger value;
    private String measure;

    public BigInteger getValue() {
        return value;
    }

    public void setValue(BigInteger value) {
        this.value = value;
    }

    public String getMeasure() {
        if (this.measure == null) {
            return "cm";
        } else {
            return this.measure;
        }
    }

    public void setMeasure(String value) {
        this.measure = value;
    }
}
